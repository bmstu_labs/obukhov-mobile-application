﻿using Obukhov.Services;
using Obukhov.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Obukhov
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockPersonDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
