﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Obukhov
{
    public interface IDialer
    {
        Task<bool> Dial(string number);
    }
}
