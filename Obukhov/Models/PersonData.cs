﻿using System;
using System.Collections.Generic;

namespace Obukhov.Models
{
    public class Passport
    {
        /// <summary>
        /// Серия
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Кем выдан
        /// </summary>
        public string IssuedBy { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime? Issued { get; set; }
    }

    /// <summary>
    /// Справочник телефонов клиентов
    /// </summary>
    public class ClientPhone
    {
        /// <summary>
        /// Id
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Является номером по умолчанию
        /// </summary>
        public bool IsMain { get; set; }
    }

        public class PersonData
    {
        public long id { get; set; }
        public string name { get; set; }
        public string address { get; set; }

        public bool unsubscribe { get; set; }
        public DateTime? sopd {  get; set; }
        
        public string middleName { get; set; }
        public string lastName { get; set; }
        
        public string inn { get; set; }

        public Passport passport { get; set; }

        public string email { get; set; }

        public string notes { get; set; }
        public DateTime birthDate { get; set; }
        public string birthPlace { get; set; }
        public string addressActual { get; set; }
        public string sex { get; set; }
        public int postIndex { get; set; }

        public List<ClientPhone> phones { get; set; }
    }
}