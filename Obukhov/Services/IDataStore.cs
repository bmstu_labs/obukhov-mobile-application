﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Obukhov.Services
{
    public interface IDataStore<T>
    {
        Task<bool> UpdateItemAsync(T item);
        Task<T> GetItemAsync(long id);
        Task<bool> DeleteItemAsync(long id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
