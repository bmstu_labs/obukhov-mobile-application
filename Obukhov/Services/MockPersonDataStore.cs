﻿using Newtonsoft.Json;
using Obukhov.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Json;
using System.Net;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization.Metadata;

namespace Obukhov.Services
{
    public class MockPersonDataStore : IDataStore<PersonData>
    {
        readonly List<PersonData> items;

        public async Task<bool> AddItemAsync(PersonData item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(PersonData item)
        {
            var oldItem = items.Where((PersonData arg) => arg.id == item.id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public Task<bool> DeleteItemAsync(string id) => throw new NotImplementedException();

        public async Task<PersonData> GetItemAsync(long id)
        {
            var client = new HttpClient();

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("https://spares.obuhov.ru/api/Clients/client?id=1010871642"),
                Headers = {                                
                    { HttpRequestHeader.Accept.ToString(), "application/json" },
                    { HttpRequestHeader.ContentType.ToString(), "application/json;charset=utf-8" },
                },
            };


            var response = client.SendAsync(requestMessage).Result;

            if (response.IsSuccessStatusCode)
            {
                try
                {
                    return await response.Content.ReadFromJsonAsync<PersonData>();
                }
                catch (Exception ex)
                {
                    string c = await response.Content.ReadAsStringAsync();
                    throw new ApplicationException("Не удалось получить данные от поставщика услуг. Обратитесь в Обухов");
                }
            }
            else
                throw new ApplicationException("Не удалось получить данные от поставщика услуг. Обратитесь в Обухов");
        }

        public Task<bool> DeleteItemAsync(long id) => throw new NotImplementedException();

        Task<IEnumerable<PersonData>> IDataStore<PersonData>.GetItemsAsync(bool forceRefresh) =>
            throw new NotImplementedException();
    }
}