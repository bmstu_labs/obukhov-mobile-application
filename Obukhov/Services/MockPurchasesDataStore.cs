﻿using Obukhov.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Obukhov.Services
{
    public class MockPurchasesDataStore : IDataStore<PurchaseData>
    {
        readonly List<PurchaseData> purchases;

        public MockPurchasesDataStore()
        {
            purchases = new List<PurchaseData>()
            {
                new PurchaseData { Id = 1, Num = 10,  },
                new PurchaseData { Id = 2, Num = 20,  },
            };
        }

        public async Task<bool> AddItemAsync(PurchaseData item)
        {
            purchases.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(PurchaseData item)
        {
            var oldItem = purchases.Where((PurchaseData arg) => arg.Id == item.Id).FirstOrDefault();
            purchases.Remove(oldItem);
            purchases.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(long id)
        {
            var oldItem = purchases.Where((PurchaseData arg) => arg.Id == id).FirstOrDefault();
            purchases.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<PurchaseData> GetItemAsync(long id)
        {
            return await Task.FromResult(purchases.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<PurchaseData>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(purchases);
        }
    }
}