﻿using Obukhov.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Obukhov.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private long itemId;
        private string text;
        private string description;
        public long Id { get; set; }

        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public long ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

        public async void LoadItemId(long itemId)
        {
            try
            {
                var item = await PersonDataStore.GetItemAsync(itemId);
                Id = item.id;
                /*Text = item.Text;
                Description = item.Description;*/
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load PersonData");
            }
        }
    }
}
