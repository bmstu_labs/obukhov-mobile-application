﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Obukhov.ViewModels
{
    [QueryProperty(nameof(Id), nameof(Id))]
    public class PersonalDataViewModel : BaseViewModel
    {
        private long id;
        private string name;
        private string lastName;
        private string middleName;
        private string address;
        private string phone;

        public Command LoadDataCommand { get; }

        public PersonalDataViewModel()
        {
            LoadDataCommand = new Command(async () => await ExecuteLoadDataCommand());
            Id = 1;
        }

        async Task ExecuteLoadDataCommand()
        {
            IsBusy = true;

            try
            {
                var data = await PersonDataStore.GetItemAsync(Id);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string LastName
        {
            get => lastName;
            set => SetProperty(ref lastName, value);
        }

        public string MiddleName
        {
            get => middleName;
            set => SetProperty(ref middleName, value);
        }

        public string Address
        {
            get => address;
            set => SetProperty(ref address, value);
        }

        public string Phone
        {
            get => phone;
            set
            {
                phone = value;
                SetProperty(ref phone, value);
            }
        }

        public long Id
        {
            get => id;
            set => LoadData();
        }

        public async void LoadData()
        {
            try
            {
                var item = await PersonDataStore.GetItemAsync(Id);
                id = item.id;
                Name = item.name;
                LastName = item.lastName;
                MiddleName = item.middleName;
                Address = item.address;
                Phone = long.Parse(item.phones.Where(p => p.IsMain).SingleOrDefault()?.Phone).ToString("+7 (###) ###-##-##");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failed to Load PersonData");
            }
        }
    }
}
