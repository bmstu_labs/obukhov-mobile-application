﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Obukhov.Views
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        private async void City_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CityPage());
        }

        private async void Dme_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DmePage());
        }
    }
}