﻿using Obukhov.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Obukhov.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CityPage : ContentPage
    {
        public CityPage()
        {
            InitializeComponent();
            this.BindingContext = new LoginViewModel();
        }

        private async void PhoneLabel_Tapped(object sender, EventArgs e)
        {
            var dialer = DependencyService.Get<IDialer>();
            if (dialer != null)
                await dialer.Dial(DialLabel.Text);
        }
    }
}