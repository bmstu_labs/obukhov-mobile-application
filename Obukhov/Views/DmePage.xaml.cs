﻿using Obukhov.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Obukhov.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DmePage : ContentPage
    {
        public DmePage()
        {
            InitializeComponent();
            this.BindingContext = new LoginViewModel();
        }

        private async void PhoneLabel_Tapped(object sender, EventArgs e)
        {
            var dialer = DependencyService.Get<IDialer>();
            if (dialer != null)
                await dialer.Dial(DialLabel.Text);
        }
    }
}