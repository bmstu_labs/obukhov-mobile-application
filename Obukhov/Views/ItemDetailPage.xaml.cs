﻿using Obukhov.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Obukhov.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}