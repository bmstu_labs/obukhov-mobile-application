﻿using Obukhov.Models;
using Obukhov.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Obukhov.Views
{
    public partial class NewItemPage : ContentPage
    {
        public PersonData Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            BindingContext = new NewItemViewModel();
        }
    }
}