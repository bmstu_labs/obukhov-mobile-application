﻿using Obukhov.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Obukhov.Views
{
    public partial class PersonalDataPage : ContentPage
    {
        PersonalDataViewModel _viewModel;
        public PersonalDataPage()
        {
            Title = "Мои данные";
            InitializeComponent();
            BindingContext = _viewModel = new PersonalDataViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //_viewModel.OnAppearing();
        }
    }
}